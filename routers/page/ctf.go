package page

import (
	"bitbucket.org/mumei/web/routers/base"
)

// AboutRouter, about page
type CTFRouter struct {
	base.BaseRouter
}

// Get method for FaqRouter.
func (this *CTFRouter) Get() {
	this.TplNames = "ctf/countdown.html"
}
