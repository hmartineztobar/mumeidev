package admin

import (
	"fmt"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"

	"bitbucket.org/mumei/web/modules/models"
	"bitbucket.org/mumei/web/modules/utils"
	"bitbucket.org/mumei/web/modules/wargame"
)

type WargameAdminRouter struct {
	ModelAdminRouter
	object models.Wargame
}

func (this *WargameAdminRouter) Object() interface{} {
	return &this.object
}

func (this *WargameAdminRouter) ObjectQs() orm.QuerySeter {
	return models.Wargames()
}

// view for create object
func (this *WargameAdminRouter) Create() {
	form := wargame.WargameAdminForm{Create: true}
	this.SetFormSets(&form)
}

// view for new object save
func (this *WargameAdminRouter) Save() {
	form := wargame.WargameAdminForm{Create: true}
	if this.ValidFormSets(&form) == false {
		return
	}

	var wargame models.Wargame
	form.SetToWargame(&wargame)
	if err := wargame.Insert(); err == nil {
		this.FlashRedirect(fmt.Sprintf("/admin/wargame/%d", wargame.Id), 302, "CreateSuccess")
		return
	} else {
		beego.Error(err)
		this.Data["Error"] = err
	}
}

// view for edit object
func (this *WargameAdminRouter) Edit() {
	form := wargame.WargameAdminForm{}
	form.SetFromWargame(&this.object)
	this.SetFormSets(&form)
}

// view for update object
func (this *WargameAdminRouter) Update() {
	form := wargame.WargameAdminForm{Id: this.object.Id}
	if this.ValidFormSets(&form) == false {
		return
	}

	// get changed field names
	changes := utils.FormChanges(&this.object, &form)

	url := fmt.Sprintf("/admin/wargame/%d", this.object.Id)

	// update changed fields only
	if len(changes) > 0 {
		form.SetToWargame(&this.object)
		if err := this.object.Update(changes...); err == nil {
			this.FlashRedirect(url, 302, "UpdateSuccess")
			return
		} else {
			beego.Error(err)
			this.Data["Error"] = err
		}
	} else {
		this.Redirect(url, 302)
	}
}

// view for list model data
func (this *WargameAdminRouter) List() {
	var wargames []models.Wargame
	qs := models.Wargames().OrderBy("Id")
	if err := this.SetObjects(qs, &wargames); err != nil {
		this.Data["Error"] = err
		beego.Error(err)
	}
}

// view for confirm delete object
func (this *WargameAdminRouter) Confirm() {
}

// view for delete object
func (this *WargameAdminRouter) Delete() {
	if this.FormOnceNotMatch() {
		return
	}

	// delete object
	if err := this.object.Delete(); err == nil {
		this.FlashRedirect("/admin/wargame", 302, "DeleteSuccess")
		//return
	} else {
		beego.Error(err)
		this.Data["Error"] = err
	}
}
