package post

import (
	"bitbucket.org/mumei/web/modules/models"
	"bitbucket.org/mumei/web/routers/base"
	"github.com/astaxie/beego/orm"
)

type ValidateWargameRouter struct {
	base.BaseRouter
}

//TODO: Mejorar algoritmo

func (this *ValidateWargameRouter) Get() {
	var user models.User
	var wargame models.Wargame
	var warlist models.WarList

	q := this.GetString("q")
	id := this.User.Id
	models.QueryTable("wargame").Filter("hash", q).One(&wargame)

	//Check if is correct or has been already done
	qs := models.QueryTable("wargame")
	correct := models.CheckIsExist(qs, "hash", q, 0)
	if correct {
		warlist.IdReto = wargame.Id
		warlist.IdUser = id
		//Add points if it was not done
		if created, _, err := orm.NewOrm().ReadOrCreate(&warlist, "IdUser", "IdReto"); err == nil {
			if created {
				err := models.Users().Filter("Id", id).One(&user)
				if err != nil {
					this.Abort("404")
					return
				}
				models.Users().Filter("Id", id).Update(orm.Params{"Points": user.Points + wargame.Points})
				models.QueryTable("wargame").Filter("hash", q).Update(orm.Params{"Users": wargame.Users + 1})
			} else {
				this.Data["Error"] = "model.error_wargame"
			}
		}
	} else {
		this.Data["Error"] = "model.error_wargame"
	}
	this.TplNames = "post/wargame.html"
}
