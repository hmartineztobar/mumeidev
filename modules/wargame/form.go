package wargame

import (
	"bitbucket.org/mumei/web/modules/models"
	"bitbucket.org/mumei/web/modules/utils"
)

type WargameAdminForm struct {
	Create bool `form:"-"`
	Id     int  `form:"-"`
	//SHA2-256
	Hash   string `valid:"Required;MinSize(64);"`
	Title  string `valid:"Required;MaxSize(255);"`
	Points int    `form:"Required;"`
	//Users that completed the wargame
	Users    int  `form:"-"`
	IsActive bool ``
}

func (form *WargameAdminForm) Labels() map[string]string {
	return map[string]string{
		"Title":  "model.wargame_title",
		"Hash":   "model.wargame_hash",
		"Points": "model.wargame_points",
	}
}

func (form *WargameAdminForm) SetFromWargame(wargame *models.Wargame) {
	utils.SetFormValues(wargame, form)
}

func (form *WargameAdminForm) SetToWargame(wargame *models.Wargame) {
	utils.SetFormValues(form, wargame, "Id")
}
