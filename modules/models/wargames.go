package models

import (
	"github.com/astaxie/beego/orm"
)

type Wargame struct {
	Id int
	//SHA2-256
	Hash   string `orm:"size(64);unqiue"`
	Title  string `orm:"size(60)"`
	Points int    ``
	//Number of Users that completed the wargame
	Users    int  ``
	IsActive bool `orm:"index"`
}

func (m *Wargame) Insert() error {
	if _, err := orm.NewOrm().Insert(m); err != nil {
		return err
	}
	return nil
}

func (m *Wargame) Read(fields ...string) error {
	if err := orm.NewOrm().Read(m, fields...); err != nil {
		return err
	}
	return nil
}

func (m *Wargame) Update(fields ...string) error {
	if _, err := orm.NewOrm().Update(m, fields...); err != nil {
		return err
	}
	return nil
}

func (m *Wargame) Delete() error {
	if _, err := orm.NewOrm().Delete(m); err != nil {
		return err
	}
	return nil
}

func Wargames() orm.QuerySeter {
	return orm.NewOrm().QueryTable("wargame").OrderBy("-Id")
}

type WarList struct {
	Id     int
	IdReto int `unqiue`
	IdUser int `unqiue`
}

func (m *WarList) Insert() error {
	if _, err := orm.NewOrm().Insert(m); err != nil {
		return err
	}
	return nil
}

func (m *WarList) Read(fields ...string) error {
	if err := orm.NewOrm().Read(m, fields...); err != nil {
		return err
	}
	return nil
}

func (m *WarList) Update(fields ...string) error {
	if _, err := orm.NewOrm().Update(m, fields...); err != nil {
		return err
	}
	return nil
}

func (m *WarList) Delete() error {
	if _, err := orm.NewOrm().Delete(m); err != nil {
		return err
	}
	return nil
}

func WarLists() orm.QuerySeter {
	return orm.NewOrm().QueryTable("war_list").OrderBy("-Id")
}

func init() {
	orm.RegisterModel(new(Wargame), new(WarList))
}
